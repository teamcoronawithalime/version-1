# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 01:51:35 2020
"""

import pandas as pd
import random

nba_parse = pd.read_csv("nbaproject2.csv")
nhl_parse = pd.read_csv("nhlproject4.csv")
pd.set_option('display.max_columns', 75)   #allows a certain amount of columns to be printed

print('Welcome to our NHL and NBA statistic calculator! ')
print('Directions: To see the stats of an NHL or NBA player, enter a name in the prompt. Stats from the current season will be displayed. ')
print()
print('Please note: Only active players will be displayed. ')
print()

class StatCalc:
    
    def __init__(self, nbaleague, nhlleague, randomp):
        """Establishes variables for the class
        
            Args:
                nbaleauge: establishes NBA object
                nhlleague: establishes NHL object
                randomp: random player object
        """
        self.nbaleague = nbaleague
        self.nhlleague = nhlleague
        self.randomp = randomp
        
        
        
    def menu(self):
        """generates a main menu for the program outlining the capabilities
        
            Args:
                menuinput(str): has the user choose what kind of data they want back, sets the program to designated mode
                
            Returns:
                print statements
        """
        print("""Welcome to our NBA and NHL stat calculator. 
              Instructions: Enter 'player' to see individual player stats from a league.
                              Enter 'compare nhl' to compare two players from within the NHL.
                              Enter 'compare nba' to compare two players from within the NBA.
                              Enter 'compare nhl and nba' to compare two players from different leagues. 
                              Enter 'random' to see a random players stats from either the NHL or NBA. 
                              Upon entering, you will be given a new prompt."""
              )
        menuinput = input('Please enter a command: ')
        if menuinput == 'player':
            return self.hold
        if menuinput == 'compare nhl':
            print(self.nhl_innercompare)
        if menuinput == 'compare nba':
            return self.nba_innercompare
        if menuinput == 'compare nhl and nba':
            return self.outercompare
        if menuinput == 'random':
            print(self.randomp)
        
        
        
    def nba_innercompare(self): #all the compare methods broken
        """compares 2 players from the NBA
        
            Args:
                compareinner1(str): first player to compare
                compareinner2(str): second player to compare
                
            Returns:
                stat lines for the two players
        """
         compareinner1 = input("Please enter a player to see his statistics: ")
         compareinner2 = input("Please enter another player to compare: ")
         if compareinner1 and compareinner2 in open('nbaproject2.csv').read():
             print(self.lowercasenba(compareinner1, compareinner2))
        
    def nhl_innercompare(self):
        """compares 2 players from the NHL
        
            Args:
                compareinner1(str): first player to compare
                compareinner2(str): second player to compare
                
            Returns:
                stat lines for the two players
        """
        compareinner3 = input("Please enter the first player to compare: ")
        compareinner4 = input("Please enter the second player to compare: ")
        if compareinner3 and compareinner4 in open('nhlproject4.csv').read():
            print(self.lowercasenhl(compareinner3, compareinner4))
        
        
    def outercompare(self):
        """compares 2 players, one from the NBA and one from the NHL
        
            Args:
                compareinner1(str): first player to compare
                compareinner2(str): second player to compare
                
            Returns:
                stat lines for the two players
        """
        compareout1 = input("Please enter a player from the NBA: ")
        compareout2 = input("Please enter a player from the NHL: ")
        if compareout1 in open('nbaproject2.csv').read():
            print(self.lowercase(compareout1))
        if compareout2 in open('nhlproject4.csv').read():
            print(self.lowercase(compareout2))
        
        
        
    def stat_helpnba(self, askuser):
        """provides a list of the stats for NBA stat line if requested by the user
        
            Args:
                Askuser(str): asks the user if they need help, if not skips the method, (also converts user input to lowercase)
                
            Returns:
                various print statements of different stats designated by the users request
        """
        askuser = askuser.lower()
        if askuser == 'yes':
            statnba = input("What basketball stat abbreviation do you need to know (for example type ppg): ")
            statnba = statnba.lower()
            if statnba == "ppg":
                return "ppg is points per game"
            elif statnba == "age":
                return "age tells how old the player is"
            elif statnba == "pos":
                return "pos is the player's position"
            elif statnba == "gp":
                return "GP is games played"
            elif statnba == "mpg":
                return "mpg is minutes per game"
            elif statnba == "min%":
                return "min% is percentage of minutes played"
            elif statnba == "usg%":
                return "usg% is Usage rate percentage"
            elif statnba == "tor%":
                return "tor% is Turnover rate percentage"
            elif statnba == "fta":
                return "FTA is free throws attempted"
            elif statnba == "ft%":
                return "FT% is free throw percentage"
            elif statnba == "2pa":
                return "2pa is two pointers attempted"
            elif statnba == "2p%":
                return "2p% is two pointer percentage"
            elif statnba == "3pa":
                return "3pa is three pointers attempted"
            elif statnba == "3p%":
                return "3p% is three pointer percentage"
            elif statnba == "efg%":
                return "eFG% is effective field goal percentage"
            elif statnba =="ts%":
                return "ts% is True Shooting percentage"
            elif statnba =="rpg":
                return "rpg is Rebounds per game"
            elif statnba =="trb%":
                return "trb% is Total Rebound Percentage"
            elif statnba =="apg":
                return "apg is assits per game"
            elif statnba =="ast%":
                return "ast% is assits percentage"
            elif statnba =="spg":
                return "spg is steals per game"
            elif statnba =="bpg":
                return "bpg is blocks per game"
            elif statnba =="topg":
                return "topg is turnovers per game"
            elif statnba =="or":
                return "OR is offensive rating"
            if askuser == 'no':
                return "The program will now continue"
    
    def stat_helpnhl(self, askusertwo):
        """provides a list of the stats for NHL stat line if requested by the user
        
            Args:
                Askusertwo(str): asks the user if they need help, if not skips the method, (also converts user input to lowercase)
                
            Returns:
                various print statements of different stats designated by the users request
        """
        askusertwo = askusertwo.lower()
        if statnhl == "G":
                return "total goals the player has scored"
            elif statnhl == "A":
                return "Total assists a player has"
            elif statnhl == "pos":
                return "pos is the player's position"
            elif statnhl == "GAMES":
                return "Games is games played"
            elif statnhl == "PTS":
                return "PTS is total points a player has totaled this year"
            elif statnhl == "+/-":
                return "+/- is a players net goal differential when on the ice"
            elif statnhl == "PIM":
                return "PIM is the total penalty time the player has gotten"
            elif statnhl == "SOG":
                return "SOG is shots on goal"
            elif statnhl == "GWG":
                return "GWG is Gaming winning goals"
            elif statnhl == "PPG":
                return "PPG is power play goals"
            elif statnhl == "PPA":
                return "PPA is Power Play Assists"
            elif statnhl == "SHG":
                return "SHG is Short handed Goals"
            elif statnhl == "HITS":
                return "HITS is total hits a player has"
            elif statnhl == "BS":
                return "BS is total blocked shots"
        if askusertwo == 'no':
            return "The program will now continue"
        

    def lowercasenba(self, playersnba):
        """converts user input player name to all lower case to search the NBA CSV file for data
            
            Args:
                playersnba(str): takes the name given by the user and converts it to all lowercase letters
            
            Returns:
                searchs corresponding CSV file for the matching player name to get the correct stats
        """
        playersnba = playersnba.lower()
        return nba_parse.query('UserInput==@playersnba')
    
    def lowercasenhl(self, playersnhl):
        """converts user input player name to all lower case to search the NHL CSV file for data
            
            Args:
                playersnhl(str): takes the name given by the user and converts it to all lowercase letters
            
            Returns:
                searchs corresponding CSV file for the matching player name to get the correct stats
        """
        playersnhl = playersnhl.lower()
        return nhl_parse.query('UserInput==@playersnhl')
    
    
    
    def exc_stathelp(self):
        """executive stat help function
        
            Args: 
                Self(str): asks the user if they need help understand any stat abbreviation in either hockey or basketball
                user will input an abbreviation if they need to understand what it means
                the function will then stat what that abbreviation means
            
            Returns:
                help for stat designations
        """
        while True:
            askuserstat = input("""
            Do you need help understanding any basketball or hockey stat abbreviations?
            Instructions: type "nba" for basketball abbreviations, 'nhl' for hockey
            abbreviations or 'no' if you don't need help. """)
            if askuserstat == 'nba':
                print(self.stat_helpnba(askuserstat))
            elif askuserstat == 'nhl':
                print(self.stat_helpnhl(askuserstat))
            else:
                break
   
    def randomp(self):
        """chooses a random player
        
            Args: 
                self(str):asks the user if they'd like to see a random player from either the nhl or nba
                opens the csv based on which league
            
            Returns:
                stat line of a random player from the nba or nhl
        """
        randomplayer = input('Would you like to see a random player from the NHL or NBA?')
        if randomplayer == self.randomp:
            if randomplayer == 'yes nba':
                with open('nbaproject2.csv') as f:
                    nbadata = f.read()
                    ranoutNBA = random.choice(nbadata)
                    print(self.lowercasenba(ranoutNBA))
            if randomplayer == 'yes nhl':
                with open('nhlproject4.csv') as f:
                    nhldata = f.read()
                    ranoutNHL = random.choice(ranoutNHL)
                    print(self.lowercasenhl(ranoutNHL))
        
    
    def hold(self):
        """holds the program to link to certain function 
        
            Args: 
                self(str): ask the user what league they'd like to select
                then it'll call that leagues function and ask the user for a player 
                the user will type a players name until they type done to stop the function
            
            Actions:
                links program to the corresponding function of the program
        """
        name = input("Please enter the league you'd to select. Instructions: type 'nba' or 'nhl' : ")
        name = name.lower()
        
        
        while name == self.nbaleague:
            nbainput = input("Please enter a player to see his statistics. Instructions - type 'done' to stop, or 'switch' to change leagues: ")
            if nbainput in open('nbaproject2.csv').read():
                print(self.lowercasenba(nbainput))
                print()
            elif nbainput == 'switch':
                return self.hold
            elif nbainput == 'done':
                print('Thank you for using our stat calculator! ')
            else:
                print("Sorry the player you have entered is not in the database. He may be retired or not currently active on any roster. ")
                print() #adds space
                print('Please type another player. ')
                nbainput = nbainput.lower()
            
            
        while name == self.nhlleague:
            nhlinput = input("Please enter a player to see his statistics. Instructions - type 'done' to stop: " )
            if nhlinput in open('nhlproject4.csv').read():
                print(self.lowercasenhl(nhlinput))
                print()
            elif nhlinput == 'switch':
                return self.hold
            elif nhlinput == 'done':
                print('Thank for using our stat calculator! ')
            else:
                print("Sorry the player you have entered is not in the database. He may be retired or not currently active on any roster. ")
                print() #adds space
                print('Please type another player.')
                nhlinput = nhlinput.lower()
    
    def restart_prgm(self): 
        """restarts/loops the project back to the beginning 
        
            Args:
                self(str): asks the user if they wants to restart the program 
                takes the input
            
            Actions:
                loops back to executive stat help method 
        """
       askrestart = input('Would you like to restart the program? ')
       if askrestart == 'yes':
           return self.exc_stathelp 

 
def main():
    """calls on the following methods of the program: menu, randomp, exc_stathelp, and hold functions
    """   
    statcalc = StatCalc('nba' , 'nhl', 'yes')
    statcalc.menu() 
    statcalc.exc_stathelp()
    statcalc.hold()

    
    
if __name__ == '__main__':
    main()